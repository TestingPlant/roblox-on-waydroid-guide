# Running Roblox on Waydroid on Arch Linux

# Installing the binder module

1. Install the `linux-zen` package ([why?](https://wiki.archlinux.org/title/Waydroid#Kernel_Modules)).
2. Configure your bootloader to use linux-zen. For example, if using GRUB, run `grub-mkconfig -o /boot/grub/grub.cfg` as root.
3. Reboot your system in order to use linux-zen.

# Installing Waydroid

1. Install [`waydroid`](https://aur.archlinux.org/packages/waydroid) from the AUR.
2. Run `waydroid init` as root.
3. Check the [GPU requirements](https://wiki.archlinux.org/title/Waydroid#GPU_Requirements).
5. Edit `/var/lib/waydroid/waydroid_base.prop` and change `ro.hardware.gralloc=gbm` to `ro.hardware.gralloc=minigbm_gbm_mesa`.
4. Run `systemctl enable --now waydroid-container`.

# Installing libndk

Run the following code:

```sh
git clone --depth=1 https://github.com/casualsnek/waydroid_script.git
cd waydroid_script
sudo python3 -B main.py -n
cd ..
rm -rf waydroid_script
```

# Starting Waydroid

Run `waydroid show-full-ui`. A window which looks like a mobile device OS will appear in about a minute. You will need to start Waydroid before playing Roblox or installing Roblox.

# Installing Roblox

1. Open the web browser in Waydroid.
2. Copy [https://d.apkpure.com/b/APK/com.roblox.client?version=latest](https://d.apkpure.com/b/APK/com.roblox.client?version=latest) into the search bar at the top. Do not press the Enter key.
3. Press the blue search button on the on screen keyboard.
4. On the prompt for "Allow Browser to access photos and media on your device", press "Allow".
5. Press the "Download" button.
6. Go to the Files application inside of Waydroid.
7. Go to the Downloads folder.
8. There should be one file in the Downloads folder. If it's still downloading, wait for the download to finish.
9. Right click the file and click "Open with".
10. A prompt saying "For your security, your phone is not allowed to install unknown apps from this source" will appear. Click "Settings".
11. Enable "Allow from this source".
12. Press the back button.
13. Click "Install".

# Using a game controller

Run the following code:

```sh
waydroid prop set persist.waydroid.udev true
waydroid prop set persist.waydroid.uevent true
```

Make sure to plug in the controller after Waydroid starts. If the controller was plugged in before Waydroid starts, the controller won't be recognized and you'll need to unplug it and plug it back in.
